# Knowledge Maps #

* A [visualization dashboard](http://kmaps.zaya.in/) developed using d3.js, resembling the pedagogy of English language as a hierarchical tree to incorporate analysis, recommendations and personalization.
* Refer the [blog](https://plp.zaya.in/english-duniya-a-personalized-learning-approach/) for more detail.
* Credentials for using the [dashboard](http://kmaps.zaya.in/):
	+ email: admin@zaya.in
	+ password: admin

### Motivation ###

* The incommensurate teacher - student ratio and inability of teachers to find every student’s weaknesses, enabled us to develop a system which can analyze and visualize the student’s performance and help him/her by recommending lessons/content.
* This can be achieved by first creating a hierarchical learning progression resembling an ideal learning path.

### Synopsis ###

* The knowledge map is the first pivotal step in the direction of personalization for English learning students, opening the floodgates to incorporate analysis, recommendations and inter-skill connections using Machine Learning techniques and Data Science insights.
* Refer the [blog](https://plp.zaya.in/english-duniya-a-personalized-learning-approach/) for more detail.

### Screenshots

![picture](public/kmaps_english.png)

![picture](public/performance_across_units.png)

### Prerequisites ###

```
meteor
d3.js
```