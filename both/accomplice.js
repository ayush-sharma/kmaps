getStudentLessonScores = function(from) {

    studentLessonObject = {};

    var distinctGrades = _.uniq(KnowledgeMapsData.find({}, {
        sort: { grade: 1 }
    }).fetch().map(function(x) {
        return x.grade;
    }), true);

    for (var gradeIndex = 0; gradeIndex < distinctGrades.length; gradeIndex++) {
        var grade = distinctGrades[gradeIndex];
        // console.log("grade", grade);

        if (typeof(grade) == "undefined") {
            continue;
        }

        var distinctSubjects = _.uniq(KnowledgeMapsData.find({ "grade": grade }, {
            sort: { subject: 1 }
        }).fetch().map(function(x) {
            return x.subject;
        }), true);

        for (var subjectIndex = 0; subjectIndex < distinctSubjects.length; subjectIndex++) {
            var subject = distinctSubjects[subjectIndex];
            // console.log("subject", subject);

            if (typeof(subject) == "undefined") {
                continue;
            }

            var distinctUnits = _.uniq(KnowledgeMapsData.find({ "grade": grade, "subject": subject }, {
                sort: { unit: 1 }
            }).fetch().map(function(x) {
                return x.unit;
            }), true);

            for (var unitIndex = 0; unitIndex < distinctUnits.length; unitIndex++) {
                var unit = distinctUnits[unitIndex];
                // console.log("unit", unit);

                if (typeof(unit) == "undefined") {
                    continue;
                }

                var distinctTopics = _.uniq(KnowledgeMapsData.find({ "grade": grade, "subject": subject, "unit": unit }, {
                    sort: { topic: 1 }
                }).fetch().map(function(x) {
                    return x.topic;
                }), true);

                for (var topicIndex = 0; topicIndex < distinctTopics.length; topicIndex++) {
                    var topic = distinctTopics[topicIndex];
                    // console.log("topic", topic);

                    if (typeof(topic) == "undefined") {
                        continue;
                    }

                    base = {
                        "grade": grade,
                        "compare": "lessons",
                        "subjects": {}
                    };
                    base["subjects"][subject] = {
                        "units": {}
                    };
                    base["subjects"][subject]["units"][unit] = {
                        "topics": {}
                    };
                    base["subjects"][subject]["units"][unit]["topics"][topic] = {};

                    // console.log('base', JSON.stringify(base));
                    classWiseScores = initClassWiseScores(base);
                    // console.log('classifiedStudents', JSON.stringify(classifiedStudents[0]));
                    // throw new Error("Something went badly wrong!");

                    var lessonCount = 0;

                    for (var lessonName in classWiseScores) {

                        lessonCount++;

                        var mean = classWiseScores[lessonName]["mean"];
                        var sr = classWiseScores[lessonName]["sr"];
                        // console.log('sr', JSON.stringify(sr));
                        // throw new Error("Something went badly wrong!");
                        var scores = classWiseScores[lessonName]["scores"];

                        for (var studentIndex = 0; studentIndex < scores.length; studentIndex++) {
                            var studentName = scores[studentIndex]["name"];
                            var studentScore = scores[studentIndex]["score"];

                            var result = "#00ff00";

                            if (studentScore < mean) {
                                result = "red";
                            }

                            if (typeof(studentLessonObject[studentName]) == "undefined" && from != "server") {
                                studentLessonObject[studentName] = { "lessonScores": [] };
                            }

                            // here, update using this

                            var updateKeyStr = "subjects." + String(subject) + ".units." + String(unit) + ".topics." + String(topic) + ".lessons." + String(sr) + ".result";

                            var queryField = {"$set": {}};
                            queryField["$set"][updateKeyStr] = result;

                            if(from == "server"){
                                StudentData.update({"name": studentName}, queryField)
                            }else{

                                var lesson = {
                                    "score": studentScore,
                                    "result": result,
                                    "grade": grade,
                                    "subject": subject,
                                    "topic": topic,
                                    "unit": unit,
                                    "sr": sr,
                                    "lessonName": lessonName
                                };

                                studentLessonObject[studentName]["lessonScores"].push(lesson);
                            }

                            console.log('grade ', gradeIndex, "/", distinctGrades.length, ", unit ", unitIndex, "/", distinctUnits.length, " topic", topicIndex, "/", distinctTopics.length, ", lessonCount", lessonCount, "/", Object.keys(classWiseScores).length, ", studentIndex", studentIndex, "/", scores.length);


                        }
                    }


                }
            }
        }
    }

    dumpStudentLesson(studentLessonObject);
}

// dumpStudentLesson = function(studentLessonObject) {
//     var count = 0;
//     for (var studentName in studentLessonObject) {
//         count++;
//         for(var lessonIndex=0; lessonIndex<studentLessonObject[studentName]["lessonScores"].length;lessonIndex++){
//             var lesson = studentLessonObject[studentName]["lessonScores"][lessonIndex];
//             StudentLessonData.insert({ "studentName": studentName, "sr": lesson["sr"], "score": lesson["score"], "result": lesson["result"], "grade": lesson["grade"], "subject": lesson["subject"], "unit": lesson["unit"], "topic": lesson["topic"], "lessonName": lesson["lessonName"] });
//             console.log('studentIndex', count, 'lessonIndex', lessonIndex, '/', studentLessonObject[studentName]["lessonScores"].length);
//         }
//     }
// }

dumpStudentLesson = function(studentLessonObject) {
    var count = 0;
    for (var studentName in studentLessonObject) {
        count++;
        console.log('inserting count ', count, Object.keys(studentLessonObject).length);
        StudentLessonData.insert({ "studentName": studentName, "lessonScores": studentLessonObject[studentName]["lessonScores"] });
    }
}