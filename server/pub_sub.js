if (Meteor.isServer){
	Meteor.publish('KnowledgeMapsData', function KnowledgeMapsDataFunc() {
		return KnowledgeMapsData.find();
	});

	Meteor.publish('StudentData', function StudentDataFunc() {
		return StudentData.find();
	});
}