if (Meteor.isClient) {

    quiz = [];
    var dq, diagLitmusMapping;
    // , result;

    // dq = {
    //     '0': {
    //         'reading': {
    //             'microstandard': 'ELL.DIA.R.1',
    //             'questions': [92421, 92423, 92425]
    //         },
    //         'grammar': {
    //             'microstandard': 'ELL.1.RE.V.10',
    //             'questions': [65294, 65295, 65298]
    //         },
    //         'listening': {
    //             'microstandard': 'ELL.1.MAL.V.91',
    //             'questions': [89887, 89922]
    //         },
    //         'vocabulary': {
    //             'microstandard': 'ELL.1.RE.V.80',
    //             'questions': [65209, 69467]
    //         }
    //     },
    //     '1': {
    //         'reading': {
    //             'microstandard': 'ELL.DIA.R.1',
    //             'questions': [78736, 92419, 92433]
    //         },
    //         'grammar': {
    //             'microstandard': 'ELL.1.RE.V.267',
    //             'questions': [68431, 89744]
    //         },
    //         'listening': {
    //             'microstandard': 'ELL.1.MAL.V.110',
    //             'questions': [92446]
    //         },
    //         'vocabulary': {
    //             'microstandard': 'ELL.1.MAL.V.09',
    //             'questions': [85226]
    //         }
    //     },
    //     '2': {
    //         'reading': {
    //             'microstandard': 'ELL.DIA.R.1',
    //             'questions': [92426, 92431, 92430]
    //         },
    //         'grammar': {
    //             'microstandard': 'ELL.1.RE.V.50',
    //             'questions': [68478, 68471]
    //         },
    //         'listening': {
    //             'microstandard': 'ELL.1.MAL.V.93',
    //             'questions': [89886, 92443]
    //         },
    //         'vocabulary': {
    //             'microstandard': 'ELL.1.RE.V.104',
    //             'questions': [65603, 65470, 70307]
    //         }
    //     },
    //     '3': {
    //         'reading': {
    //             'microstandard': 'ELL.DIA.R.1',
    //             'questions': [71163, 71166, 92415]
    //         },
    //         'grammar': {
    //             'microstandard': 'ELL.1.RE.V.267',
    //             'questions': [89743, 89746]
    //         },
    //         'listening': {
    //             'microstandard': 'ELL.1.MAL.V.110',
    //             'questions': [92444, 92445]
    //         },
    //         'vocabulary': {
    //             'microstandard': 'ELL.1.RE.V.227',
    //             'questions': [65210, 65212, 65559, 66435, 67214, 67215, 89971, 71369]
    //         }
    //     }
    // }
    // console.log('dq', dq);

    // var srToQuestion = {};
    // for (var litmusNo in dq) {
    //     for (var skill in dq[litmusNo]) {
    //         for (var i = 0; i < dq[litmusNo][skill]["questions"].length; i++) {
    //             srToQuestion[dq[litmusNo][skill]["questions"][i]] = { "level": litmusNo, "skill": skill, "microstandard": dq[litmusNo][skill]["microstandard"] };
    //         }
    //     }
    // }
    // console.log('srToQuestion', srToQuestion);

    // diagLitmusMapping = {};

    // for (var litmusNo in dq) {
    //     for (var skill in dq[litmusNo]) {
    //         if (diagLitmusMapping[skill] != undefined) {
    //             diagLitmusMapping[skill][parseInt(litmusNo)] = dq[litmusNo][skill];
    //         } else {
    //             diagLitmusMapping[skill] = {};
    //             diagLitmusMapping[skill][parseInt(litmusNo)] = dq[litmusNo][skill];
    //         }
    //     }
    // }

    // diagLitmusMapping = {'reading': {'1': {'microstandard': 'ELL.1.RE.V.118', 'questions': ['111f3f9f-0726-4fe2-9f09-c897ba162b53', '7e1622c6-761b-4ab5-9a7c-9cc67cb6b0ef']}, '0': {'microstandard': 'ELL.1.WR.AP.22', 'questions': ['b89fc070-d415-4949-854d-591bcaf4f8ab', '7643f8ce-dbc8-416e-b857-247e89acb6fe', '5159ad70-e439-4df0-9035-8a6a497495af', '25a3aeee-b15f-442a-adc4-9ae163e87e40', '51be15df-2539-421b-97d7-916e5f1fe472', '8490ce28-2a35-4862-acec-406aa149909a', 'ccb12161-5f5f-45a4-9857-0f3fa7762b83']}, '3': {'microstandard': 'ELL.DIA.R.4', 'questions': ['368e4be2-ec0d-45dc-bb66-9a149886388e', '06a0fdc5-0ffd-416f-ab37-fe9fddecfda1']}, '2': {'microstandard': 'ELL.DIA.R.2', 'questions': ['2063e6e2-040a-4507-a51f-6f087401cbb6', '3cdb4ffd-c44d-4975-b422-b2450a620e44', 'b06d642c-fe00-4ac0-ba05-c13617574b2b']}}, 'grammar': {'1': {'microstandard': 'ELL.1.RE.V.10', 'questions': ['dd9d17bb-7611-4fff-bb9c-d97337150142', 'e1c4993c-77d4-47dc-91a1-43baf51718cc', 'bd30db17-d366-4778-ac44-3ebeef84acce']}, '0': {'microstandard': 'ELL.1.RE.V.50', 'questions': ['5b9fa591-f41c-4634-b4e9-74518a162ef0']}, '2': {'microstandard': 'ELL.1.RE.V.50', 'questions': ['c4ba98a7-f6d2-4599-bf5f-e7a4c1f54ca3', '019e59a3-4c4e-4f2e-ac1d-1bb69af047d2']}}, 'vocabulary': {'1': {'microstandard': 'ELL.1.RE.V.14', 'questions': ['8e714817-186f-46ac-a521-7bf0c70aaf5d', '4eb50a81-bbe8-49c4-8efb-e284008d5f7f', '5b85c60b-4976-4440-b381-1f2346b1d816']}, '0': {'microstandard': 'ELL.1.RE.V.31', 'questions': ['54ada475-ce87-411b-a075-60572a36a111', 'c5875c4c-58dd-4e7c-ab60-431ff27baa66', 'd6418ab6-ea0a-40be-9680-e53cd83b23e4', 'ae3b3692-c121-4767-bcc3-60a4eb92b88a', '81903b9b-99b3-4573-9e5b-43dfc8543b89', '092d355e-fef9-493b-99da-47a6cf74c045']}, '3': {'microstandard': 'ELL.1.MAL.V.08', 'questions': ['6d116141-c026-408d-8320-4daf805887a9', '2f7d3a76-c58a-4e7e-a658-55994c166f3c', '83a5450b-4754-4025-ae99-9bf309b8a428']}, '2': {'microstandard': 'ELL.1.RE.V.72', 'questions': ['82e3f767-6b16-4425-89c4-ac00dc4c557c', '58db5325-ef81-4ad8-ac57-e4d280fbd1bc', 'e7c18a27-457f-422e-975b-b28ddd62fb87']}}, 'listening': {'1': {'microstandard': 'ELL.1.MAL.V.85', 'questions': ['f76a0a14-023c-4dad-a48c-580ee6e14af0', '039bdd80-afc4-42fe-bb71-00adbb3b7ff4']}, '0': {'microstandard': 'ELL.1.MAL.V.85', 'questions': ['bf7648c4-4f20-4c91-aa7a-a412883317d7']}, '3': {'microstandard': 'ELL.1.MAL.V.110', 'questions': ['8af50c71-0799-422c-9fb1-295c1ab1eb0b', '07fddf47-c997-478a-99ce-3a6d20c0dc04', 'af9a7f9f-e6f7-4c96-8ecc-2ac528ad2adb']}, '2': {'microstandard': 'ELL.1.MAL.V.93', 'questions': ['f848c1e2-88d9-41a0-b9a1-26dd875a3392', 'b7ac6b9a-5423-41b9-8cfb-a4a9efa94ecc', '3a967e76-48dd-4b8c-a52c-4f42ff1c5ab2']}}};

    // console.log('diagLitmusMapping', diagLitmusMapping);

    displayQuestionDq = function(res, quiz) {
        console.log('in function displayQuestionDq');
        document.getElementById("dispQuestion").innerHTML = ""
        if (res == null) {
            clearClassNames("radioAnswer");
            document.getElementById("dispQuestion").innerHTML += "<br><b>Diagnosis Complete</b>";
            postDiagnosisTest(quiz);
            return 0;
        }
        var q_sr = res.qSr;
        var question = DiagnosticQuestionsData.findOne({ "_id": q_sr });

        if (question == undefined) {
            console.log('question not found with _id', q_sr);
            return 0;
        }

        clearClassNames("radioAnswer");

        document.getElementById("dispQuestion").innerHTML += "<br><b>" + String(q_sr) + "</b> - " + String(question.lesson_name) + ", <br><b>Level</b> - " + String(question.level) + "<br> <b>sr</b> - " + String(question.sr) + ", <b>answer</b> - " + String(question.answer);
        document.getElementById("dispQuestion").innerHTML += "<br><br>" + String(question.q_title)

        for (var i = 0; i < question.options.length; i++) {
            document.getElementById("dispQuestion").innerHTML += "<br><br><input class='radioAnswer' name='group1' answer='" + String(question.answer) + "' type='radio' id='" + String.fromCharCode(97 + i) + "' /> <label class='radioAnswer' for='" + String.fromCharCode(97 + i) + "'>" + question.options[i] + "</label>";
        }
    }

    function displaySuggestedSr(level_one, test) {
        console.log('in function displaySuggestedSr');
        var test_one = test[0];
        var oldqSet = test_one["qSet"];
        console.log('oldqSet', oldqSet);
        var qSet = {};
        for (var i in oldqSet) {
            qSet[i - level_one] = { "answered": oldqSet[i]["answered"] };
            if (oldqSet[i]["sr"] != undefined) {
                qSet[i - level_one]["sr"] = oldqSet[i]["sr"];
            } else {
                qSet[i - level_one]["sr"] = oldqSet[i]["qSr"];
            }
        }
        console.log('qSet', qSet);
        var newQSet = {};
        // array = [];
        var last = null;
        for (var i = 0; i <= 3; i++) {
            if (last == null) {
                last = parseInt(level_one) * -1;
            } else {
                last++;
            }
            // array.push(last);
            if (qSet[last] == undefined) {
                // try {
                    var srGroup = diagLitmusMapping[test_one["skill"]][last + level_one]["questions"];
                    newQSet[last] = { "sr": srGroup[Math.floor(Math.random() * (srGroup.length)) + 0], "answered": "NA" , "skill": test[0]["skill"], "level": last + level_one};
                // }catch(err) {
                //     console.log('err', err);
                //     return test;
                // }
            } else {
                newQSet[last] = { "sr": qSet[last]["sr"], "answered": qSet[last]["answered"] , "skill": test[0]["skill"], "level": last}
            }
        }
        console.log('newQSet', newQSet);
        var suggestedQ = getSuggestedSr2(newQSet)[0];
        console.log('suggestedQ', suggestedQ);
        var q_data = DiagnosticQuestionsData.findOne({ "_id": suggestedQ });
        if (q_data != undefined) {
            var suggestedSr = q_data["sr"];
            var suggestedLessonName = q_data["lesson_name"];
            console.log('suggestedSr', suggestedSr);
            // document.getElementById("dispQuestion").innerHTML += " - suggestedSr " + String(suggestedSr) + " suggestedLessonName "+ suggestedLessonName;
        }
        quiz.push(newQSet);
        if (test.length > 1) {
            if (suggestedQ != undefined) {
                console.log('hereeeee');
                test[1]["level"] = parseInt(DiagnosticQuestionsData.findOne({ "_id": suggestedQ })["level"]);
                // test[1]["level"] = parseInt(srToQuestion[suggestedQ]["level"]);
            } else {
                test[1]["level"] = Object.keys(diagLitmusMapping[test[1]["skill"]]).length - 1;
            }
        }
        return test;
    }

    var getNextQSr = function(test) {
        try{
            console.log('in function getNextQSr');
            console.log('in getNextQSr', test[0]);
            if (test.length > 0) {
                if (test[0]["count"] >= 2) {
                    console.log('slicing');
                    test = displaySuggestedSr(test[0]["level"], test);
                    var newTest = test.slice(1, test.length);
                    return getNextQSr(newTest);
                }
                if (test[0]["previousAnswer"] == null) {
                    if (diagLitmusMapping[test[0]["skill"]][test[0]["level"]] != undefined) {
                        console.log('if 1');
                        var q_set = diagLitmusMapping[test[0]["skill"]][test[0]["level"]]["questions"];
                        if(q_set.length == 0){
                            console.log('q_set empty');
                            var newTest = test.slice(1, test.length);
                            return getNextQSr(newTest);
                        }
                        return { "skill": test[0]["skill"], "qSr": q_set[Math.floor(Math.random() * (q_set.length)) + 0], "test": test, "actualLevel": test[0]["level"], "microstandard": diagLitmusMapping[test[0]["skill"]][test[0]["level"]]["microstandard"] };
                    } else {
                        console.log('else 1');
                        test = displaySuggestedSr(test[0]["level"], test);
                        var newTest = test.slice(1, test.length);
                        return getNextQSr(newTest);
                    }
                } else if (test[0]["previousAnswer"] == 0) {
                    if (diagLitmusMapping[test[0]["skill"]][test[0]["level"] - 2] != undefined) {
                        console.log('if 2');
                        var q_set = diagLitmusMapping[test[0]["skill"]][test[0]["level"] - 2]["questions"];
                        if(q_set.length == 0){
                            console.log('q_set empty');
                            var newTest = test.slice(1, test.length);
                            return getNextQSr(newTest);
                        }
                        var intermediate_q_set = diagLitmusMapping[test[0]["skill"]][test[0]["level"] - 1]["questions"];
                        test[0]["qSet"][test[0]["level"] - 1] = { "qSr": intermediate_q_set[Math.floor(Math.random() * (intermediate_q_set.length)) + 0], "answered": "NA" };
                        return { "skill": test[0]["skill"], "qSr": q_set[Math.floor(Math.random() * (q_set.length)) + 0], "test": test, "actualLevel": test[0]["level"] - 2, "microstandard": diagLitmusMapping[test[0]["skill"]][test[0]["level"] - 2]["microstandard"] };
                    } else {
                        console.log('else 2');
                        test = displaySuggestedSr(test[0]["level"], test);
                        var newTest = test.slice(1, test.length);
                        return getNextQSr(newTest);
                    }
                } else if (test[0]["previousAnswer"] == 1) {
                    if (diagLitmusMapping[test[0]["skill"]][test[0]["level"] + 2] != undefined) {
                        console.log('if 3');
                        var q_set = diagLitmusMapping[test[0]["skill"]][test[0]["level"] + 2]["questions"];
                        if(q_set.length == 0){
                            console.log('q_set empty');
                            var newTest = test.slice(1, test.length);
                            return getNextQSr(newTest);
                        }
                        var intermediate_q_set = diagLitmusMapping[test[0]["skill"]][test[0]["level"] + 1]["questions"];
                        test[0]["qSet"][test[0]["level"] + 1] = { "qSr": intermediate_q_set[Math.floor(Math.random() * (intermediate_q_set.length)) + 0], "answered": "NA" };
                        return { "skill": test[0]["skill"], "qSr": q_set[Math.floor(Math.random() * (q_set.length)) + 0], "test": test, "actualLevel": test[0]["level"] + 2, "microstandard": diagLitmusMapping[test[0]["skill"]][test[0]["level"] + 2]["microstandard"] };
                    } else {
                        console.log('else 3');
                        test = displaySuggestedSr(test[0]["level"], test);
                        var newTest = test.slice(1, test.length);
                        return getNextQSr(newTest);
                    }
                }
            } else {
                console.log('diagnosis complete');
                return null;
            }
        }catch(err){
            console.log('err big', err);
            var newTest = test.slice(1, test.length);
            return getNextQSr(newTest);
        }
    }

    postDiagnosisTest = function(quiz) {
        console.log('in function postDiagnosisTest');
        var studentName = document.getElementById("studentName").value;
        recommendationsWithPrereqs = runDiagnostic(quiz, studentName);

        // recommendationsWithPrereqs = {"vocabulary":[{"sr":"ffa7b369-050c-46e5-ba33-58c8c38c8417","prereqs":[],"sr_name":"rich  poor  cheap  expensive","prereqs_names":[]},{"sr":"59e53fcd-b0e5-438d-9e89-9ace057b93ea","prereqs":[],"sr_name":"pencil  book  pen  paper","prereqs_names":[]},{"sr":"8a1169f8-7a68-49a8-a52a-f6d87fd41673","prereqs":[],"sr_name":"water  soda  milk  juice  coffee  tea","prereqs_names":[]},{"sr":"abafdeab-f41a-4546-9d1c-02ba1ec7d376","prereqs":[],"sr_name":"hair  nose  eye  teeth","prereqs_names":[]},{"sr":"bd180bce-2f63-4c4b-8490-1ffb81fa3b30","prereqs":[],"sr_name":"banana  grapes  apple  orange","prereqs_names":[]},{"sr":"66835336-0e26-4f11-8610-86b1b7801d87","prereqs":[],"sr_name":"student  teacher  headmaster  headmistress  friend","prereqs_names":[]},{"sr":"d73592d7-e241-4abe-a849-1508e264651c","prereqs":[],"sr_name":"take this  give me that  come here  go there","prereqs_names":[]},{"sr":"f6c61162-8e84-4265-bbb6-ffae1b035b05","prereqs":[],"sr_name":"what is your name","prereqs_names":[]},{"sr":"3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","prereqs":[],"sr_name":"the alphabet","prereqs_names":[]},{"sr":"876ad289-03ea-45d9-8f56-ff07a880a402","prereqs":["59e53fcd-b0e5-438d-9e89-9ace057b93ea","8a1169f8-7a68-49a8-a52a-f6d87fd41673","abafdeab-f41a-4546-9d1c-02ba1ec7d376","bd180bce-2f63-4c4b-8490-1ffb81fa3b30"],"sr_name":"more than  less than","prereqs_names":["pencil  book  pen  paper","water  soda  milk  juice  coffee  tea","hair  nose  eye  teeth","banana  grapes  apple  orange"]}],"reading":[{"sr":"3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","prereqs":[],"sr_name":"the alphabet","prereqs_names":[]},{"sr":"4f133ef2-b56e-4c92-bd76-29412ec58aba","prereqs":[],"sr_name":"mother  father  brother  sister","prereqs_names":[]},{"sr":"1e14f7c6-1a4e-4375-8a6e-584f749b8df9","prereqs":[],"sr_name":"grandmother  grandfather  uncle  aunt","prereqs_names":[]},{"sr":"e344f39b-59bb-498c-896d-2a1e6609c4e7","prereqs":[],"sr_name":"parents  grandparents  son  daughter","prereqs_names":[]},{"sr":"52f8decd-1dbf-4191-87c4-097e6b819bf2","prereqs":[],"sr_name":"this vs that","prereqs_names":[]},{"sr":"552e1d19-56c5-492b-aa1d-66a28d256627","prereqs":["3f9a680f-38a9-4b67-95c4-ea3915bb4ee7"],"sr_name":"alphabetical order","prereqs_names":["the alphabet"]},{"sr":"13a2b14f-4898-46c0-bd37-4007775f3951","prereqs":["3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","552e1d19-56c5-492b-aa1d-66a28d256627","33aec5ae-c131-4896-8ebc-30fe0b7f712d"],"sr_name":"letter names  letter to name","prereqs_names":["the alphabet","alphabetical order","letter names  name to letter"]},{"sr":"ce3741e8-c989-49fb-8167-9e34c990d007","prereqs":["3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","552e1d19-56c5-492b-aa1d-66a28d256627","33aec5ae-c131-4896-8ebc-30fe0b7f712d","13a2b14f-4898-46c0-bd37-4007775f3951"],"sr_name":"introduction","prereqs_names":["the alphabet","alphabetical order","letter names  name to letter","letter names  letter to name"]},{"sr":"9fcf6c1d-e116-4c8f-9186-b1571ad50adf","prereqs":["3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","552e1d19-56c5-492b-aa1d-66a28d256627","33aec5ae-c131-4896-8ebc-30fe0b7f712d","13a2b14f-4898-46c0-bd37-4007775f3951","ce3741e8-c989-49fb-8167-9e34c990d007"],"sr_name":"the alphabet","prereqs_names":["the alphabet","alphabetical order","letter names  name to letter","letter names  letter to name","introduction"]},{"sr":"85512e54-4098-4e55-aeb9-23fcee2a0b2b","prereqs":["3f9a680f-38a9-4b67-95c4-ea3915bb4ee7","552e1d19-56c5-492b-aa1d-66a28d256627","33aec5ae-c131-4896-8ebc-30fe0b7f712d","13a2b14f-4898-46c0-bd37-4007775f3951","ce3741e8-c989-49fb-8167-9e34c990d007","9fcf6c1d-e116-4c8f-9186-b1571ad50adf"],"sr_name":"alphabetical order","prereqs_names":["the alphabet","alphabetical order","letter names  name to letter","letter names  letter to name","introduction","the alphabet"]}],"grammar":[{"sr":"49d420b3-7201-463e-90b4-74974b6e0329","prereqs":[],"sr_name":"man  woman  boy  girl","prereqs_names":[]},{"sr":"e10f9ae7-917c-4241-9274-8b0d1ad3505b","prereqs":[],"sr_name":"house  school  shop  park","prereqs_names":[]},{"sr":"adbea24e-b75a-466b-bdfe-60671d8540e3","prereqs":[],"sr_name":"cat  dog  cow  bird","prereqs_names":[]},{"sr":"9a61e02b-4495-44ab-ae91-49a34859f332","prereqs":[],"sr_name":"person  place  animal  thing","prereqs_names":[]},{"sr":"bd180bce-2f63-4c4b-8490-1ffb81fa3b30","prereqs":[],"sr_name":"banana  grapes  apple  orange","prereqs_names":[]},{"sr":"022380a2-8265-46bc-8ad1-f54cf118565c","prereqs":[],"sr_name":"sun  moon  sky  star","prereqs_names":[]},{"sr":"cf2147f6-6a80-4adc-89c7-7052bebe37ff","prereqs":[],"sr_name":"numbers   1 10","prereqs_names":[]},{"sr":"9ae5fda5-b5ac-4851-8e0a-32e9034047b5","prereqs":[],"sr_name":"come  go  stand  sit","prereqs_names":[]},{"sr":"b938d73c-42fc-4d53-8725-d5868a2bd67f","prereqs":["59e53fcd-b0e5-438d-9e89-9ace057b93ea","49d420b3-7201-463e-90b4-74974b6e0329","e10f9ae7-917c-4241-9274-8b0d1ad3505b","adbea24e-b75a-466b-bdfe-60671d8540e3","9a61e02b-4495-44ab-ae91-49a34859f332"],"sr_name":"the noun  naming words","prereqs_names":["pencil  book  pen  paper","man  woman  boy  girl","house  school  shop  park","cat  dog  cow  bird","person  place  animal  thing"]},{"sr":"76b39594-a4ba-4feb-87bd-a4907528b4fb","prereqs":["59e53fcd-b0e5-438d-9e89-9ace057b93ea","49d420b3-7201-463e-90b4-74974b6e0329","e10f9ae7-917c-4241-9274-8b0d1ad3505b","adbea24e-b75a-466b-bdfe-60671d8540e3","9a61e02b-4495-44ab-ae91-49a34859f332","bd180bce-2f63-4c4b-8490-1ffb81fa3b30","b938d73c-42fc-4d53-8725-d5868a2bd67f"],"sr_name":"indefinite articles     a    vs    an","prereqs_names":["pencil  book  pen  paper","man  woman  boy  girl","house  school  shop  park","cat  dog  cow  bird","person  place  animal  thing","banana  grapes  apple  orange","the noun  naming words"]}],"listening":[{"sr":"49d420b3-7201-463e-90b4-74974b6e0329","prereqs":[],"sr_name":"man  woman  boy  girl","prereqs_names":[]},{"sr":"e10f9ae7-917c-4241-9274-8b0d1ad3505b","prereqs":[],"sr_name":"house  school  shop  park","prereqs_names":[]},{"sr":"adbea24e-b75a-466b-bdfe-60671d8540e3","prereqs":[],"sr_name":"cat  dog  cow  bird","prereqs_names":[]},{"sr":"9a61e02b-4495-44ab-ae91-49a34859f332","prereqs":[],"sr_name":"person  place  animal  thing","prereqs_names":[]},{"sr":"bd180bce-2f63-4c4b-8490-1ffb81fa3b30","prereqs":[],"sr_name":"banana  grapes  apple  orange","prereqs_names":[]},{"sr":"022380a2-8265-46bc-8ad1-f54cf118565c","prereqs":[],"sr_name":"sun  moon  sky  star","prereqs_names":[]},{"sr":"cf2147f6-6a80-4adc-89c7-7052bebe37ff","prereqs":[],"sr_name":"numbers   1 10","prereqs_names":[]},{"sr":"9ae5fda5-b5ac-4851-8e0a-32e9034047b5","prereqs":[],"sr_name":"come  go  stand  sit","prereqs_names":[]},{"sr":"b938d73c-42fc-4d53-8725-d5868a2bd67f","prereqs":["59e53fcd-b0e5-438d-9e89-9ace057b93ea","49d420b3-7201-463e-90b4-74974b6e0329","e10f9ae7-917c-4241-9274-8b0d1ad3505b","adbea24e-b75a-466b-bdfe-60671d8540e3","9a61e02b-4495-44ab-ae91-49a34859f332"],"sr_name":"the noun  naming words","prereqs_names":["pencil  book  pen  paper","man  woman  boy  girl","house  school  shop  park","cat  dog  cow  bird","person  place  animal  thing"]},{"sr":"76b39594-a4ba-4feb-87bd-a4907528b4fb","prereqs":["59e53fcd-b0e5-438d-9e89-9ace057b93ea","49d420b3-7201-463e-90b4-74974b6e0329","e10f9ae7-917c-4241-9274-8b0d1ad3505b","adbea24e-b75a-466b-bdfe-60671d8540e3","9a61e02b-4495-44ab-ae91-49a34859f332","bd180bce-2f63-4c4b-8490-1ffb81fa3b30","b938d73c-42fc-4d53-8725-d5868a2bd67f"],"sr_name":"indefinite articles     a    vs    an","prereqs_names":["pencil  book  pen  paper","man  woman  boy  girl","house  school  shop  park","cat  dog  cow  bird","person  place  animal  thing","banana  grapes  apple  orange","the noun  naming words"]}]};

        clearRecommendationTable();

        var rTable = document.getElementById('recommendations');
        var tRow = rTable.tHead.insertRow();
        var col1Head = tRow.insertCell();
        col1Head.innerHTML = "<center><b>Skill</b></center>";
        col1Head.style.width = '50%';
        var col2Head = tRow.insertCell();
        col2Head.innerHTML = "<center><b>Suggested Sr</b></center>";
        col2Head.style.width = '50%';
        var col3Head = tRow.insertCell();
        col3Head.innerHTML = "<center><b>Prereqs</b></center>";
        col3Head.style.width = '50%';

        var tBody = rTable.tBodies[0];

        for(var skill in recommendationsWithPrereqs){

            for(var i=0;i<recommendationsWithPrereqs[skill].length;i++){
                var tRow = tBody.insertRow();

                if(i == 0){
                    var cell1 = tRow.insertCell();
                    cell1.innerHTML = "<b>" + skill + "</b>";
                    cell1.style.width = '50%';
                    cell1.rowSpan = recommendationsWithPrereqs[skill].length;
                }

                var cell2 = tRow.insertCell();
                cell2.innerHTML = recommendationsWithPrereqs[skill][i]["sr_name"];
                cell2.style.width = '50%';

                var cell3 = tRow.insertCell();
                cell3.innerHTML = "";
                for(var j=0;j<recommendationsWithPrereqs[skill][i]["prereqs_names"].length;j++){
                    cell3.innerHTML += recommendationsWithPrereqs[skill][i]["prereqs_names"][j] + ",";
                }
                cell3.style.width = '50%';
            }

            
        }
    }

    function clearClassNames(className) {
        console.log('in function clearClassNames');
        var list = document.getElementsByClassName(className);
        for (var k = list.length - 1; k >= 0; k--) {
            var item = list[k];
            item.parentNode.removeChild(item);
        }
    }

    function clearRecommendationTable(){
        console.log('in function clearRecommendationTable');
        var rTable = document.getElementById('recommendations');
        while (rTable.tHead.length > 0) {
            rTable.tHead.deleteRow(0);
        }
        while (rTable.rows.length > 0) {
            rTable.deleteRow(0)
        }
    }

    Template.dqDemo.events({
        "click #startTest": function() {

            // dq = DiagnosticQuestionsMappingsData.findOne({ "type": "diagnostic_questions" })["value"];
            diagLitmusMapping = DiagnosticQuestionsMappingsData.findOne({ "type": "diagnostic_litmus_mapping" })["value"];


            var realTimeGrade = document.getElementById("realTimeGrade").value;
            if (realTimeGrade == "") {
                alert('enter realTimeGrade');
                return 0;
            }

            quiz = [];
            // $("#recommendations").empty();

            clearRecommendationTable();

            document.getElementById("dispQuestion").innerHTML = "";
            clearClassNames("radioAnswer");

            function setPreviousAnswerCallback(tests, x){
                tests["previousAnswer"] = x[0];
                tests["count"]++;
                // console.log('tests', tests);
                // if(x[0] == 1){
                //     x[1].test[0]["qSet"][x[1]["actualLevel"]] = { "sr": x[1].qSr, "answered": "right" };
                // }else{
                //     x[1].test[0]["qSet"][x[1]["actualLevel"]] = { "sr": x[1].qSr, "answered": "wrong" };
                // }
            }

            var initializeTest = [
            {
                "skill": "vocabulary",
                "qSet": {},
                "level": parseInt(realTimeGrade),
                "previousAnswer": null,
                "actualLevel": 0,
                "count": 0,
                set setPreviousAnswer(x) {
                    setPreviousAnswerCallback(this, x);
                }
            }, 
            {
                "skill": "reading",
                "qSet": {},
                "level": parseInt(realTimeGrade),
                "previousAnswer": null,
                "actualLevel": 0,
                "count": 0,
                set setPreviousAnswer(x) {
                    setPreviousAnswerCallback(this, x);
                }
            }, 
            {
                "skill": "grammar",
                "qSet": {},
                "level": parseInt(realTimeGrade),
                "previousAnswer": null,
                "actualLevel": 0,
                "count": 0,
                set setPreviousAnswer(x) {
                    setPreviousAnswerCallback(this, x);
                }
            }, {
                "skill": "listening",
                "qSet": {},
                "level": parseInt(realTimeGrade),
                "previousAnswer": null,
                "actualLevel": 0,
                "count": 0,
                set setPreviousAnswer(x) {
                    setPreviousAnswerCallback(this, x);
                }
            }];

            result = getNextQSr(initializeTest);
            displayQuestionDq(result, quiz);
        },
        "change .radioAnswer": function(e) {
            var userAnswer = e.currentTarget.id;
            var actualAnswer = e.currentTarget.getAttribute("answer");
            if (userAnswer == actualAnswer) {
                result.test[0]["setPreviousAnswer"] = [1, result];
                result.test[0]["qSet"][result["actualLevel"]] = { "sr": result.qSr, "answered": "right" };
            } else {
                result.test[0]["setPreviousAnswer"] = [0, result];
                result.test[0]["qSet"][result["actualLevel"]] = { "sr": result.qSr, "answered": "wrong" };
            }
            result = getNextQSr(result.test);
            displayQuestionDq(result, quiz)

        }
    });

}


